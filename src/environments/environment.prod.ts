export const environment = {
  production: true,
  API_URL: 'http://snc.cultura.gov.br/api/v2/sistemadeculturalocal/',
  googleAnalyticsKey: 'UA-3584649-45'
};
